#!/bin/bash
set -e
cd $(dirname $(realpath $0))
echo "Create builder Image"
docker build -t vue .
echo "Create builder Container"
docker create --name vue vue
echo "Copy Static files from builder Container"
docker cp vue:/app/dist app/
echo "Remove the builder Container"
docker container rm vue
echo "Build Done !"
echo "Remove builder Image by hand if need with following cmd:"
echo "docker image rm vue"
